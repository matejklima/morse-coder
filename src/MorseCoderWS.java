import logic.Coder;
import utils.MailSender;

import javax.ws.rs.*;

/**
 * This is the main controller class that summons the requests and handles them.
 *
 * Created by Matej on 06.07.17.
 */
@Path("/code")
public class MorseCoderWS {

    // The Java method will process HTTP GET requests
    // The Java method will produce content identified by the MIME Media type "text/plain"
    @Produces("text/plain")
    @GET
    @Path("/")
    public String getClichedMessage(
            @QueryParam("from") String from,
            @QueryParam("to") String to,
            @QueryParam("text") String text) {
        // Return some cliched textual content
        String coded = Coder.code(text);
        String response = "";
        response = response.concat(from);
        response = response.concat("\n");
        response = response.concat(to);
        response = response.concat("\n");
        response = response.concat("\n");
        response = response.concat("Result: ");
        response = response.concat(coded);
        MailSender.send(from, to, response);
        return response;

    }
}