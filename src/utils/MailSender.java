package utils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Properties;

/**
 * Created by Matej on 06.07.17.
 */
public class MailSender {
    private static String sender = "telegram-lvicat@seznam.cz";
    private static String host = "smtp.seznam.cz";
    private static String uname = "telegram-lvicat@seznam.cz";
    private static String psw = "nebeskarybna80";
    Properties properties = System.getProperties();

//    private static String host = "smtp.seznam.cz";
//    private static String uname = "telegram-lvicat";
//    private static String psw = "nebeskarybna80";
//    Properties properties = System.getProperties();

    public static void send(String from, String to, String text) {
        String recipient = "telegram-lvicat@seznam.cz";
        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", "smtp.seznam.cz");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "25");
        props.put("mail.debug", "true");
        props.put("mail.smtp.socketFactory.port", "25");
        //props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.socketFactory.fallback", "false");
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(uname, psw);
                    }
                });

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(sender));

            // Set To: header field of the header.
//            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));

            // Set Subject: header field
            message.setSubject("Telegram pro táborníka jménem " + to);

            // Now set the actual message
            message.setText(text);

            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
        } catch (MessagingException e) {
            System.out.println(e);
        }
    }

}
