package logic;

import java.text.Normalizer;

/**
 * Created by Matej on 06.07.17.
 */
public class Coder {

    private static String czechify(String text) {
        return Normalizer.normalize(text.toLowerCase(), Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }


    public static String code(String text) {
        System.out.println("ORIGINAL: " + text);
        String temp = czechify(text);
        System.out.println("CZECHIFIED: " + temp);
        char[] input = temp.toCharArray();

        CoderLogic logic = new CoderLogic();

        for (char anInput : input) {
            logic.addChar(anInput);
        }

        return logic.end();
    }
}

