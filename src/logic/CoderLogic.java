package logic;

/**
 * Dictionary for characters, sentences etc.
 */
class CoderLogic {

    private StringBuilder output = new StringBuilder("");

    void addChar(char ch) {
        translate(ch);
        output.append("|");
    }


    String end() {
        output.append("||");
        return output.toString();
    }

    private void translate(char ch) {
        switch (ch) {
            case ' ':
                output.append("");
                break;
            case '.':
                output.append("  STOP  ");
                break;
            case ',':
                output.append("  STOP  ");
                break;
            case 'a':
                output.append(".-");
                break;
            case 'b':
                output.append("-...");
                break;
            case 'c':
                output.append("-.-.");
                break;
            case 'd':
                output.append("-..");
                break;
            case 'e':
                output.append(".");
                break;
            case 'f':
                output.append("..-.");
                break;
            case 'g':
                output.append("--.");
                break;
            case 'h':
                output.append("....");
                break;
            case 'i':
                output.append("..");
                break;
            case 'j':
                output.append(".---");
                break;
            case 'k':
                output.append("-.-");
                break;
            case 'l':
                output.append(".-..");
                break;
            case 'm':
                output.append("--");
                break;
            case 'n':
                output.append("-.");
                break;
            case 'o':
                output.append("---");
                break;
            case 'p':
                output.append(".--.");
                break;
            case 'q':
                output.append("--.-");
                break;
            case 'r':
                output.append(".-.");
                break;
            case 's':
                output.append("...");
                break;
            case 't':
                output.append("-");
                break;
            case 'u':
                output.append("..-");
                break;
            case 'v':
                output.append("...-");
                break;
            case 'w':
                output.append(".--");
                break;
            case 'x':
                output.append("-..-");
                break;
            case 'y':
                output.append("-.--");
                break;
            case 'z':
                output.append("--..");
                break;
            default:
                output.append(ch);
                break;
        }

    }
}
